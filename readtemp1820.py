#!/usr/bin/python3
# Created by Konrad "WebS1821" Polys, webs at k13.pl, January 2015 
# Some code snippets come from https://learn.adafruit.com/adafruits-raspberry-pi-lesson-11-ds18b20-temperature-sensing/software

import glob
import subprocess
import sys
import os.path
import pickle
from time import sleep

#########
#Config:
config_filename = "temp.conf"
sensor_prefix = "10" #According to https://github.com/bcl/digitemp "10" for DS1820/DS18S20, "28" for DS18B20
verbose = False
#output in:
celsius = True
fahrenheit = False
#########


config_file = "./" + config_filename
def main(argv):
    global config_file
    config_file = os.path.dirname(argv[0]) + "/" + config_filename
    if (len(argv) == 2):
        if (argv[1] == "init"):
            print("Initializing...")
            init()
        else:
            if (argv[1].isdigit()):
                if verbose:
                    print("Reading Temp "+argv[1])
                read_temp(int(argv[1]))
            else:
                print("Type 'init' or sensor number")
                sys.exit(2)
    else:
        print("One argument is needed")
        sys.exit(1)

def init():
    global config_file
    dir = '/sys/bus/w1/devices/'
    device = glob.glob(dir + sensor_prefix + '*')
    for d in range(len(device)):
        print(device[d]+" as Temp "+str(d))
        device[d] += '/w1_slave'
    f = open(config_file, 'wb')
    pickle.dump(device, f)
    f.close()

def read_temp(i):
    global config_file
    devices=[]
    if os.path.isfile(config_file):
        if verbose:
            print("Opening config file...")
        f = open(config_file, 'rb')
        try:
            devices = pickle.load(f)
            if (i > len(devices)-1):
                print("Number beyond count of available devices")
                sys.exit(4)
            else:
                out=read_raw_temp(devices[i])
                while out[0].strip()[-3:] != 'YES':
                    sleep(0.2)
                    out=read_raw_temp(devices[i])
                equals_pos = out[1].find('t=')
                if equals_pos != -1:
                    temp_string = out[1][equals_pos+2:]
                    temp_c = float(temp_string) / 1000.0
                    if celsius:
                        print(temp_c)
                    if fahrenheit:
                        temp_f = temp_c * 9.0 / 5.0 + 32.0
                        print(temp_f)
        finally:
            f.close()
    else:
        print("Initialize first!")
        sys.exit(3)

def read_raw_temp(dev):
    catdata = subprocess.Popen(['cat',dev], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out,err = catdata.communicate()
    out_decode = out.decode('utf-8')
    lines = out_decode.split('\n')
    return lines

if __name__ == "__main__":
    main(sys.argv)
