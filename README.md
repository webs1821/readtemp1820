# readtemp1820 #
This script is intended to read Dallas DS18x20 temp sensors on 1-wire bus.
Script needs Python 3. It's tested on Banana Pi with Bananian but should work also on Raspberry Pi. 


**How to install:**
1. You need "git" package, on Debian: sudo apt-get install git
2. git clone https://bitbucket.org/webs1821/readtemp1820

**How to use:**
1. ./readtemp1820.py init
This command will create config file with connected sensors and assigned number. Should looks similar like this:
Initializing...
/sys/bus/w1/devices/10-000802bf0950 as Temp 0
/sys/bus/w1/devices/10-000802bf87e0 as Temp 1
/sys/bus/w1/devices/10-000802bf957d as Temp 2
/sys/bus/w1/devices/10-000802bf6efd as Temp 3

2. for read the temp you only need to run script with sensor number, for example:  ./readtemp1820.py 0 
It will read temperature from sensor 0, eg. /sys/bus/w1/devices/10-000802bf0950

Some config is available in readtemp1820.py